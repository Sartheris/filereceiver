﻿using System;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Net.Sockets;
using System.Threading;

namespace FileReceiver {

    public partial class MainForm : Form {

        private NetworkStream stream;
        private TcpClient client;
        private string fileName;
        private FileStream fileStream;

        public MainForm() {
            InitializeComponent();
        }

        protected override async void OnShown(EventArgs e) {
            TcpListener listener = TcpListener.Create(8080);
            listener.Start();
            while (true) {
                client = await listener.AcceptTcpClientAsync();
                new Thread(new ThreadStart(() => handleClient())).Start();
            }
        }

        private void handleClient() {
            stream = client.GetStream();
            string identity = getIdentification();
            if (identity.Equals("Sartheris")) {
                // permission granted
                stream.WriteByte(1);
            } else {
                // permission denied
                stream.WriteByte(0);
                return;
            }
            int i = stream.ReadByte();
            if (i == 0) {
                receiveRawData();
            } else if (i == 1) {
                receiveFile();
            }
            closeConnection();
        }

        private string getIdentification() {
            byte[] byteIdentity = new byte[9];
            stream.Read(byteIdentity, 0, 9);
            string s = ASCIIEncoding.ASCII.GetString(byteIdentity);
            return s;
        }

        private void receiveRawData() {

        }

        private void receiveFile() {
            getFileInfo();
            getFile();
        }

        private void getFileInfo() {
            byte[] fileNameBytes;
            byte[] fileNameLengthBytes = new byte[4];
            stream.Read(fileNameLengthBytes, 0, 4);
            fileNameBytes = new byte[BitConverter.ToInt32(fileNameLengthBytes, 0)];
            stream.Read(fileNameBytes, 0, fileNameBytes.Length);
            fileName = ASCIIEncoding.ASCII.GetString(fileNameBytes);
        }

        private void getFile() {
            fileStream = File.Open(@"C:\ErrorLog\" + fileName, FileMode.Create);
            int read;
            int totalRead = 0;
            byte[] buffer = new byte[32 * 1024];
            while ((read = stream.Read(buffer, 0, buffer.Length)) > 0) {
                fileStream.Write(buffer, 0, read);
                totalRead += read;
            }
        }

        private void closeConnection() {
            fileStream.Dispose();
            client.Close();
            //stream.Dispose();
            MessageBox.Show("File successfully received");
        }
    }
}